import os

from random import randint

from flask import Flask
import redis


CACHE_LIVE_TIME_SEC = 10
HOST = '127.0.0.1'
PORT = 8000
REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379


app = Flask(__name__)
cache = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)


@app.route('/', methods=['GET'])
def main():
    number = cache.get('number')
    if number:
        return {'number': int(number)}

    number = randint(1, 10)
    cache.set('number', number, ex=CACHE_LIVE_TIME_SEC)

    return {'number': int(number)}


if __name__ == '__main__':
    app.run(host=HOST, port=PORT)
